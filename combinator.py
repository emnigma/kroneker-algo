from itertools import product

def permutations_generator(array_of_arrays):
    return product(*[i for i in array_of_arrays])