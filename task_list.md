

Что сделать:
есть полином f(x) степени n
0. Произведем избавление от квадратных множителей (модуль готов, не включен в алго)
0.5. Проверим, являются ли случайно выбранные корни решением

Модуль A - готов
1. Возьмем целую часть от ведущей степени, деленную пополам, округлим вверх, назовем m
2. Возьмем m целых чисел: a_1, a2, ... , a_m,
3. найдем значения f(a_i) , где i: 1...m

Модуль B - готов
4. создадим по массиву делителей каждого f(a_i): A_i

Модуль C - готов
5. Создадим все комбинации, выбирая по одному делителю из каждого массива
(то есть будет m элементов, p_1 будет из A_1, p_2 будет из A_2 и т.д.)

Модуль D1 - готов
Написать реализацию подсчета интерполяционного многочлена Лагранжа

Модуль D2 - готов (not tested)
6. составим из каждого массива p многочлен лагранжа L_j (j - возможное число перестановок)

Модуль D3 - готов (not tested)
7. для каждого L_j проверим, является ли он делителем f
8. Если является - вывести на экран

## [ ] - when generating function vals, make shure there is no function zeros between plugged values. for now kg._generate_function_vals() will throw exception if it happends

Example:
```
g = sp.Poly(x**7 - 2*x + 1)
kg._generate_function_vals()=[1, 0, 2, 125]
kg._generate_divisors_combinations()=[[1], [], [1, 2], [1, 5, 25, 125]]
```

## [ ] - IT IS REQUIRED to implement square-freeing

## [+] - what exactly is a divisor?

Example:

what divisors ```1``` is supposed to have? ```[1, -1]```? and ```-1```?

> divisible: ```4``` => divisors: ```[1, -1, 2, -2, 4, -4]```

## [ ] - Restrict polynomial degree (!!!)

Может, проблема в том, что kg._generate_divisors_combinations()
может попасться []


## [+] - What's with this situation?

```
lp = LagrangePolynimial(
    (1, 1, 1),
    [0, 1, -1]
)
coeffitients = lp.L().all_coeffs()
print(coeffitients)

print(lp.L())
```

