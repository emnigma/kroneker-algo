import numpy as np

def main():
    p1 = np.polynomial.Polynomial([-9, 0, 1])
    p2 = np.polynomial.Polynomial([1, 2, 3])
    print(p1 + p2)

    # print(roots)

if __name__ == "__main__":
    main()