import sympy as sp


def PSQFF(polynome: sp.Poly):
    _, factor_tuples = sp.sqf_list(polynome)

    return list(map(lambda x: sp.Poly(x[0]), factor_tuples))

def PSQFF_by_hand(polynome: sp.Poly):

    # initialization
    r: sp.Poly = sp.gcd(polynome, polynome.diff())
    t = polynome / r

    e, j = 1, 1

    factors = []

    while True:

        if r == 1:
            e = j
            factors.append(t)
            break
        
        v = sp.gcd(r, t)

        print(f"t / v: {sp.simplify(t / v)}")

        factors.append(sp.simplify(t / v))

        r = r / v

        if sp.rem(r, v) != 0:
            if r.degree <= v:
                return 

        r = sp.simplify(r)

        # r = sp.rem(r, v)
        print(f'remainder:{r}')
        t = v
        j += 1
    
    return factors
