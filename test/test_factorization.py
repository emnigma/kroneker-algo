import os
import sys
import unittest

import sympy as sp

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from sympy_polynomials import KroneckerFactoriser

class TestKroneckerFactorizerMethods(unittest.TestCase):

    def test0(self):
        x = sp.symbols("x")
        test_p = sp.Poly(x**4 + 4)
    
        kf = KroneckerFactoriser(test_p, [0, 1, -1])

        # for item in kf.list_interpolated_polys_for_zero_remainder():
        #     print(item)

    def test1(self):
        x = sp.symbols("x")
        test_p = sp.Poly(x**5 - x**4 - 2*x**3 - 8*x**2 + 6*x - 1)
    
        kf = KroneckerFactoriser(test_p, [1, -1, 2])

        print(kf.list_interpolated_polys_with_zero_remainder())

    def test2(self):
        x = sp.symbols("x")
        test_p = sp.Poly(x**2 - 9)
    
        kf = KroneckerFactoriser(test_p, [1, -1])

        print(kf.list_interpolated_polys_with_zero_remainder())

if __name__ == "__main__":
    unittest.main()