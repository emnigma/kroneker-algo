import os
from re import A
import sys
import unittest

import sympy as sp

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from sympy_polynomials import LagrangePolynimial, x

class TestLagrangeMethods(unittest.TestCase):
    
    def test_l_j_generation(self):
        lp = LagrangePolynimial([0, 1], [1, 2])

        self.assertEqual(lp._l(0), (x - 1) / -1)
    
    def test_simple_imported_interpolation(self):
        poly = sp.Poly(sp.interpolate(data=[1, 4, 9], x=[1, 2, 3]))
        
        coeffs = poly.all_coeffs()

        self.assertEqual(len(coeffs), 3)

        self.assertEqual(coeffs[0], 1)
        self.assertEqual(coeffs[1], 0)
        self.assertAlmostEqual(coeffs[2], 0)
    
    def test_simple_L_generation(self):
        lp1 = LagrangePolynimial([1, 2, 3], [1, 4, 9])
        self.assertEqual(lp1.L(), sp.Poly(x**2, domain="QQ"))
    
    def test_medium_L_generation(self):
        # wiki
        lp = LagrangePolynimial(
            [-1.5, -0.75, 0, 0.75, 1.5],
            [-14.1014, -0.931596, 0, 0.931596, 14.1014]
        )
        coeffitients = lp.L().all_coeffs()

        self.assertEqual(len(coeffitients), 4)

        self.assertAlmostEqual(coeffitients[0], 4.834848, 5)
        self.assertEqual(coeffitients[1], 0)
        self.assertAlmostEqual(coeffitients[2], -1.477474, 5)
        self.assertEqual(coeffitients[3], 0)
    
    def _test_question(self):
        # wiki
        lp = LagrangePolynimial(
            [0, 1, -1],
            (1, 1, 1),
        )

        self.assertEqual(lp.L().all_coeffs(), [1])


if __name__ == "__main__":
    unittest.main()