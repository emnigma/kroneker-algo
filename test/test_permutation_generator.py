import os
import sys
import unittest

import sympy as sp

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from combinator import permutations_generator
from itertools import product

class TestKroneckerFactorizerMethods(unittest.TestCase):

    def test_value_generator(self):
        vals1 = [0, 1, -1]
        vals2 = [0, 1]
        
        array_of_vals = [vals1, vals2]
        should_yield = [(0, 0), (0, 1), (1, 0), (1, 1), (-1, 0), (-1, 1)]

        self.assertEqual(list(permutations_generator(array_of_vals)), should_yield)


if __name__ == "__main__":
    unittest.main()
