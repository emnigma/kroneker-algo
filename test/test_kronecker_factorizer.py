import os
import sys
import unittest

import sympy as sp

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from sympy_polynomials import KroneckerFactoriser

class TestKroneckerFactorizerMethods(unittest.TestCase):

    def _test_value_generator(self):
        self.assertEqual(list(KroneckerFactoriser.val_generator(4)), [0, 1, -1, 2])

    def _test_function_vals_generation_with_default_values(self):
        x = sp.symbols("x")

        f = sp.Poly(x**3 + 2*x**2 - x + 1)
        kf = KroneckerFactoriser(f, [0, 1])
        self.assertEqual(kf._compute_function_vals(), [1, 3])

        g = sp.Poly(x**7 - 2*x + 2)
        kg = KroneckerFactoriser(g, [0, 1, -1, 2])
        self.assertEqual(kg._compute_function_vals(), [2, 1, 3, 126])
    
    def _test_function_vals_generation_with_given_values(self):
        x = sp.symbols("x")

        t = sp.Poly(x**3 + 2*x**2 - x + 1)
        kf = KroneckerFactoriser(t, [0, 1])
        self.assertEqual(kf._compute_function_vals(), [1, 3])

        g = sp.Poly(x**7 - 2*x + 2)
        kg = KroneckerFactoriser(g, [0, 1, -1, 2])
        self.assertEqual(kg._compute_function_vals(), [2, 1, 3, 126])
    
    def _test_divisors_arrays_for_func_val_generation_default(self):
        x = sp.symbols("x")

        g = sp.Poly(x**7 - 2*x + 2)
        kg = KroneckerFactoriser(g)
        
        # x: [0, 1, -1, 2])
        # y: [2, 1, 3, 126]
        predicted_divisors = [[1, 2], [1], [1, 3], [1, 2, 3, 6, 7, 9, 14, 18, 21, 42, 63, 126]]

        self.assertEqual(kg._compute_divisors_for_function_vals(), predicted_divisors)
    
    def test_combinations_generator_default(self):
        x = sp.symbols("x")

        f = sp.Poly(3*x**3 + 2*x**2 - x + 1)
        kf = KroneckerFactoriser(f, [0, 1])
        
        # x: [0, 1]
        # y: [1, 5]
        predicted_divisors_combinations = [
            (1, 1), (1, -1), (1, 5), (1, -5),
            (-1, 1), (-1, -1), (-1, 5), (-1, -5)
        ]

        self.assertEqual(
            list(kf._divisor_combinations_generator()),
            predicted_divisors_combinations
        )

    def test_existing_solutions_remover(self):
        x = sp.symbols("x")

        f = sp.Poly((x - 1)**3 * (x + 1)**2)
        kf = KroneckerFactoriser(f, [2, 3, 4])

        print(kf.exact_input_values)
        print(kf.polynomial)
        # print(kf._remove_solutions_if_present())
        # print(kf.polynomial)
        for item in kf.list_interpolated_polys_with_zero_remainder():
            print(item)

if __name__ == "__main__":
    unittest.main()
