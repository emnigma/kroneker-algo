import os
import sys
import unittest

import sympy as sp

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from divisor import get_divisors

class TestKroneckerFactorizerMethods(unittest.TestCase):

    def test_one(self):
        self.assertEqual(get_divisors(1), [1, -1])

    def test_positive_divisors(self):
        self.assertEqual(get_divisors(24), [1, -1, 2, -2, 3, -3, 4, -4, 6, -6, 8, -8, 12, -12, 24, -24])
    
    def test_negative_divisors(self):
        self.assertEqual(get_divisors(-24), [1, -1, 2, -2, 3, -3, 4, -4, 6, -6, 8, -8, 12, -12, 24, -24])


if __name__ == "__main__":
    unittest.main()
