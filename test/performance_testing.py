import os
import random
import sys
import sympy as sp
import time
import pandas as pd

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from constants import x

from sympy_polynomials import KroneckerFactoriser

def measure_method_time_if_not_throws(method):
    try:
        start = time.time()
        method()
        end = time.time()
        return end - start
    except RuntimeError:
        print("some error thrown")
    return None

def get_random_degree():
    random_degree = 0

    while random_degree == 0:
        random_degree = random.randint(-50, 50)
    
    return random_degree

def create_random_poly(with_degree):
    return sp.Poly([get_random_degree() for _ in range(with_degree)], x)

class PerformanceTest:

    def __init__(self, sample_size, degree, chosen_factorizer) -> None:
        self.sample_size = sample_size
        self.test_results = []
        self.chosen_factorizer_class = chosen_factorizer

        self.test_performance(degree)

    def test_performance(self, degree):
        
        if not self.test_results:
            self.test_results = []

            print("Testing samples...")

            while len(self.test_results) < self.sample_size:
                random_poly = create_random_poly(degree)
                p = self.chosen_factorizer_class(random_poly)
                kronecker_method = p.list_interpolated_polys_with_zero_remainder

                def multivariate_factorization_method():
                    return sp.factor(random_poly)

                kroneker_time = measure_method_time_if_not_throws(kronecker_method)
                sympy_time = measure_method_time_if_not_throws(multivariate_factorization_method)

                if kroneker_time and sympy_time:
                    self.test_results.append(
                        {
                            "kroneker_time": kroneker_time,
                            "sympy_time": sympy_time
                        }
                    )
                    print(f"-- Testing {len(self.test_results)}/{self.sample_size} polynomial. Kroneker time = {kroneker_time}, Sympy time = {sympy_time}")

            print("Sample generation finished")

    def get_kroneker_avg(self):
        return sum(list(item["kroneker_time"] for item in self.test_results)) / len(self.test_results)

    def get_sympy_avg(self):
        return sum(list(item["sympy_time"] for item in self.test_results)) / len(self.test_results)

if __name__ == "__main__":
    count = 20

    kroneker_avgs = []
    sympy_avgs = []

    degrees = [3, 4, 5, 6, 7]

    for degree in degrees:
        tester = PerformanceTest(count, degree, KroneckerFactoriser)

        kroneker_avgs.append(tester.get_kroneker_avg())
        sympy_avgs.append(tester.get_sympy_avg())

    df = pd.DataFrame(zip(degrees, kroneker_avgs, sympy_avgs), columns=["Polynomial degree", "Kroneker avg", "Sympy avg"])
    df.to_csv("./result_df2.csv", index=False)