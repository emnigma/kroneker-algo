import os
import sys
import unittest

import sympy as sp
from sympy.polys.polytools import poly

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from square_free import PSQFF

class TestKroneckerFactorizerMethods(unittest.TestCase):

    def test_psqff_simple_poly(self):
        x = sp.symbols("x")

        polynome = sp.Poly(2 * (x**2 - 2*x + 1))

        self.assertEqual(PSQFF(polynome), [sp.Poly(x - 1)])

    def test_psqff_medium_poly(self):
        x = sp.symbols("x")

        polynome = sp.Poly((x - 1)**4 * (x + 2)**2)

        self.assertCountEqual(PSQFF(polynome), [sp.Poly(x - 1), sp.Poly(x + 2)])

    def _test_imported(self):
        x = sp.symbols("x")

        polynome = x**2 - 2*x + 1

        # factors = map(lambda tuple: tuple[0], sp.sqf_list(polynome))

        _, factor_tuples = sp.sqf_list(polynome)
        
        print(list(map(lambda x: sp.Poly(x[0]), factor_tuples)))

if __name__ == "__main__":
    unittest.main()
