import sympy as sp
from constants import x

class Point:
    def __init__(self, a, b) -> None:
        self.x = a
        self.y = b

class LagrangePolynimial:

    def __init__(self, array_x, array_w) -> None:
        if len([i for i in array_x if array_x.count(i) == 1]) != len(array_x):
            raise RuntimeError("x values should not be the same!")

        self.points = [Point(item[0], item[1]) for item in zip(array_x, array_w)]
    
    def _l(self, j):
        l_j = 1
        for index, point in enumerate(self.points):
            if index == j:
                continue
            else:
                next_mul = (x - point.x) / (self.points[j].x - point.x)
                l_j *= next_mul
        
        return l_j
    
    def L(self):
        L = 0
        for index, point in enumerate(self.points):
            L += point.y * sp.Poly(self._l(index))

        return sp.Poly(L)
