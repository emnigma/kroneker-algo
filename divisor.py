from sympy import divisors

def get_divisors(num):

    divs = divisors(num)

    all_divs = []
    for item in divs:
        all_divs.append(item)
        all_divs.append(-item)
    return all_divs
    