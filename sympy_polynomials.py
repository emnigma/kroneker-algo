import sympy as sp
from sympy.polys.polyfuncs import interpolate

from constants import x
from divisor import get_divisors
from combinator import permutations_generator
from lagrange import LagrangePolynimial
from square_free import PSQFF

def get_desirable_m(polynomial_degree):

    if polynomial_degree <= 1:
        raise RuntimeError(f"Unexpected argument: {polynomial_degree}")

    if polynomial_degree % 2 == 0:
        return polynomial_degree / 2 + 1
    else:
        return (polynomial_degree - 1) / 2 + 1

class KroneckerFactoriser:

    def __init__(self, symbol_polynome: sp.Poly, exact_input_values=None) -> None:
        self.polynomial = symbol_polynome

        if exact_input_values:
            if len(exact_input_values) != get_desirable_m(sp.degree(self.polynomial)):
                raise RuntimeError(f"Number of given inputs is wrong, should be: {get_desirable_m(sp.degree(self.polynomial))}, got {len(exact_input_values)}")
            self.exact_input_values = exact_input_values
        else:
            self.exact_input_values = self._get_default_input_vals()
    
    def val_generator(num):
        current_value = 0
        for _ in range(num):
            if current_value == 0:
                yield 0
                current_value += 1
                continue
            if current_value > 0:
                yield current_value
                current_value = -current_value
                continue
            if current_value < 0:
                yield current_value
                current_value -= 1
                current_value = -current_value
                continue
        return
    
    def _get_default_input_vals(self):
        m = get_desirable_m(sp.degree(self.polynomial))
        return [val for val in KroneckerFactoriser.val_generator(m)]

    def _compute_function_vals(self):

        new_exact = []
        f_vals = []

        new_poly = self.polynomial.copy()

        for val in self.exact_input_values:
            if new_poly.eval(val) == 0:
                new_poly, _rem = sp.div(new_poly, sp.Poly(x - val))
        
        self.polynomial = new_poly

        f_vals = [self.polynomial.eval(val) for val in self.exact_input_values]

        if 0 in f_vals:
            raise RuntimeError(f"There is a solution between given inputs! Handling not yet implemented. Given inputs: {f_vals}")
        return f_vals
    
    def _compute_divisors_for_function_vals(self):
        # делители

        function_vals = self._compute_function_vals()
        return [get_divisors(function_val) for function_val in function_vals]

    def _divisor_combinations_generator(self):
        # декартово произведение: все возможные упорядоченные пары элементов из массивов делителей 
        divs = self._compute_divisors_for_function_vals()
        return permutations_generator(divs)
    
    def interpolate_points(self, comb):
        # вспомогательный метод
        points = list(zip(self.exact_input_values, comb))
        interpolated = sp.Poly(sp.interpolate(points, x), x)

        return interpolated
    
    def list_interpolation_polynomials_from_divs_generator(self):
        # интерполяция по наборам точек
        
        return filter(
            lambda poly: poly.domain == sp.ZZ,
            map(
                lambda div_combination: self.interpolate_points(div_combination),
                self._divisor_combinations_generator()
            )
        )
    
    def list_interpolated_polys_with_zero_remainder(self):
        # проверить полиномы на нулевой остаток

        poly_list = []

        current_poly = self.polynomial.copy()

        for interpolated_poly in self.list_interpolation_polynomials_from_divs_generator():
            if sp.rem(current_poly, interpolated_poly) == 0 and interpolated_poly.degree() != 0:
                poly_list.append(interpolated_poly)

                current_poly, _rem = sp.div(current_poly, interpolated_poly)

        if current_poly.degree() != 0:
            poly_list.append(current_poly)

        return poly_list

        return list(
            filter(
                lambda x: sp.rem(self.polynomial, x) == 0 and x.degree() != 0,
                self.list_interpolation_polynomials_from_divs_generator()
            )
        )
        

def main():
    polynomials = [
        x**4 + 4,
        x**5 - x**4 - 2*x**3 - 8*x**2 + 6*x - 1,
        x**3 - 1
    ]

    for item in polynomials:
        item = PSQFF(item)

    for item in polynomials:
        k = KroneckerFactoriser(sp.Poly(item, x))

        print("Список полиномов, полученный алгоритмом:")
        print(*k.list_interpolated_polys_with_zero_remainder(), sep="\n")
        print(f'Проверка: {sp.factor(item)=}')


if __name__ == "__main__":
    main()